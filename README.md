git config --global user.name "Aneek Banerjee"
git config --global user.email "4021743-Cynical_Joker@users.noreply.gitlab.com"

Create a new repository
git clone git@gitlab.com:Cynical_Joker/taleofmankind.git
cd taleofmankind
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:Cynical_Joker/taleofmankind.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:Cynical_Joker/taleofmankind.git
git push -u origin --all
git push -u origin --tags